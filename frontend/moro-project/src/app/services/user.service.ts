import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { User } from '../model/user';
import { LoginCredentials } from '../model/login-credentials';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {

  // private usersUrl = 'api/users'; 
  private usersUrl = '//localhost:8080';  // URL to web api

  constructor(
    private http: HttpClient) { }

  getUsers (): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl+'/getAllUsers')
      .pipe(
        tap(users => this.log(`fetched users`)),
        catchError(this.handleError('getUsers', []))
      );
  }

  addUser (user: User): Observable<User> {
    let url = `${this.usersUrl}/createUser`;
    return this.http.post<User>(url, user, httpOptions).pipe(
      tap((user: User) => this.log(`added user w/ id=${user.id}`)),
      catchError(this.handleError<User>('addUser'))
    );
  }

  deleteUser (user: User | number, credentials: LoginCredentials): Observable<User> {
    const id: number = typeof user === 'number' ? user : user.id;

    let httpOptionsAuth = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + btoa(`${credentials.username}:${credentials.password}`) })
    };

    const url = `${this.usersUrl}/secured/deleteUser?id=${id}`;

    return this.http.get<User>(url, httpOptionsAuth).pipe(
      tap(_ => this.log(`deleted user id=${id}`)),
      catchError(this.handleError<User>('deleteUser'))
    );
  }

  updateUser (user: User): Observable<any> {
    return this.http.post<User>(this.usersUrl+'/updateUser/', user, httpOptions).pipe(
      tap(_ => this.log(`updated user id=${user.id}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  /** Log a UserService message with the MessageService */
  private log(message: string) {
    // this.messageService.add('UserService: ' + message);
    console.log(message);
  }
}