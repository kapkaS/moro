import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../model/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})

export class UserDetailComponent implements OnInit {
  @Input() user: User;
  @Output() valueChange = new EventEmitter();

  constructor(private userService: UserService) { }

  ngOnInit(): void { }

  goBack(): void {
    this.valueChange.emit('');
  }

 save(name): void {
    this.userService.updateUser({id: this.user.id, name, password: this.user.password, username: this.user.username})
      .subscribe(() => this.goBack());
  }
}
