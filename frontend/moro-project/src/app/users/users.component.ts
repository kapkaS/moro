import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { UserService } from '../services/user.service';
import { LoginCredentials } from '../model/login-credentials';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users;
  selectedUser: User;

  loginCredentials: LoginCredentials;
  showNewUserInputs: boolean;

  constructor(private userService: UserService) { }

  setCredentials(username: string, password: string) {
    this.loginCredentials = new LoginCredentials(username, password);
  }

  refresh(){
    this.selectedUser = undefined;
    this.getUsers();
  }

  onSelect(user: User): void {
    this.selectedUser = user;
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this.userService.getUsers()
        .subscribe(users => this.users = users);
  }

  delete(user: User): void {
    this.userService.deleteUser(user, this.loginCredentials).subscribe(() => this.getUsers() );
  }

  add(name: string, username: string, password: string): void {
    name = name.trim();
    if (!name) { return; }
    this.userService.addUser({ name, username, password } as User)
      .subscribe(hero => this.getUsers());
      this.showNewUserInputs = false;
  }

  newUser() {
    this.showNewUserInputs = true;
  }
}
