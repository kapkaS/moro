import { User } from './model/user';

export const USERS: User[] = [
  { id: 11, name: 'Mr. Nice', password: '', username:'' },
  { id: 12, name: 'Narco', password: '', username:'' },
  { id: 13, name: 'Bombasto', password: '', username:'' },
  { id: 14, name: 'Celeritas', password: '', username:'' },
  { id: 15, name: 'Magneta', password: '', username:'' },
  { id: 16, name: 'RubberMan', password: '', username:'' },
  { id: 17, name: 'Dynama', password: '', username:'' },
  { id: 18, name: 'Dr IQ', password: '', username:'' },
  { id: 19, name: 'Magma', password: '', username:'' },
  { id: 20, name: 'Tornado', password: '', username:'' }
];