package backend.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Represents an User for this web application.
 */
@Entity
@Table(name = "users")
public class User {

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(unique = true, nullable = false)
  private long id;
    
  @NotNull
  @Size(min=2, max=30, message="Validation constraint = name must have lenght between 2 and 30")
  private String name;


  private String username;
  
  private String password;

  // ------------------------
  // PUBLIC METHODS
  // ------------------------
  
  public User() { }

  public User(long id) { 
    this.id = id;
  }

  public User(String name) {
    this.name = name;
  }

  public long getId() {
    return id;
  }

  public void setId(long value) {
    this.id = value;
  }

  public String getName() {
    return name;
  }

  public void setName(String value) {
    this.name = value;
  }
  
  public String getUsername() {
	  return username;
  }
  
  public void setUsername(String value) {
	  this.username = value;
  }
  
  public String getPassword() {
	  return password;
  }
  
  public void setPassword(String value) {
	  this.password = value;
  }
  
} // class User
