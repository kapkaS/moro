package backend.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import backend.models.User;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class MyRestController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/getUser", method = RequestMethod.GET)
	@ResponseBody
	public User readIdReturnUser(@RequestParam(value = "id") long id) {
		return userService.getUserById(id);
	}

	@RequestMapping(value = "/getAllUsers", method = RequestMethod.GET)
	@ResponseBody
	public List<User> returnAllUsers () {
		return userService.getAllUsers();
	}
	

	@RequestMapping(value = "/createUser", method = RequestMethod.POST)
	public void createUser (@Valid @RequestBody  User user) {
		userService.createUser(user);
	}

	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	public void updateUser (@Valid @RequestBody  User user) {
		userService.updateUser(user.getId(), user.getName());
			//user.getId() is id of user to be updated, user.getName() is the new name
	}
	
}
