package backend.controllers;

import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import backend.models.User;
import backend.models.UserDao;
import backend.exceptions.NotFoundException;
import backend.exceptions.UniqueNameException;

@Service
public class UserService {

	// Wire the UserDao used inside this controller.
	@Autowired
	private UserDao userDao;

	public User getUserById(long id) {
		User user = userDao.getUserById(id);
		if (user == null ) throw new NotFoundException();
		return user;
	}

	public List<User> getAllUsers () {
		List<User> usersList = userDao.getAll();
		return usersList;
	}
	
	public void createUser (User newUser) {
//		List<User> usersList = userDao.getAll();
//		boolean nameAlreadyExists = usersList.stream().anyMatch(user -> name.equals(user.getName()));
//		if (nameAlreadyExists) throw new UniqueNameException();

		userDao.create(newUser);
	}

	public void updateUser (long id, String name) {
		User user = this.getUserById(id);
		user.setName(name);
		userDao.update(user);
	}
	
	public void deleteUser (long id) {
		User user = this.getUserById(id);
		userDao.delete(user);
	}

}