package backend.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import backend.models.User;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class RemoveRestController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/secured/deleteUser", method = RequestMethod.GET)
	public void deleteUser(@RequestParam(value = "id") long id) {
		System.out.println("REMOVE");
		userService.deleteUser(id);
	}
	//TODO : after removing the user from DB, logout
}
